const express = require("express")

// Create a Router instance that functions as a routing system

const router = express.Router()

// Import the taskControllers

const taskControllers = require("../controllers/taskControllers")

// Route to get all the tasks

router.get("/", (req, res) => {
	taskControllers.getAllTasks().then(resultFromController => res.send(resultFromController))
})

// Route to create a task

router.post("/", (req, res) => {
	taskControllers.createTask(req.body).then(resultFromController => res.send(resultFromController))
})

// Route to delete a task

router.delete("/:id", (req, res) => {
	taskControllers.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

// Route to update a task

router.put("/:id", (req, res) => {
	taskControllers.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})

// Activity

router.get("/:id", (req, res) => {
	taskControllers.getSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

router.put("/:id/complete", (req, res) => {
	taskControllers.updateStatus(req.params.id).then(resultFromController => res.send(resultFromController))
})

// Export the router object to be used in index.js

module.exports = router